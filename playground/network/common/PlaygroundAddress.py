'''
Created on Nov 25, 2013

@author: sethjn
'''

from Error import InvalidPlaygroundAddressString, InvalidPlaygroundFormat
from twisted.internet.interfaces import IAddress
import random

class PlaygroundAddressType(object):
    @classmethod
    def ConvertStringPart(cls, part):
        return part
    
    @classmethod
    def FromString(cls, addressString):
        if type(addressString) != str:
            raise InvalidPlaygroundAddressString("Address string not of type string")
        
        parts = addressString.split(".")
        if len(parts) != 4:
            raise InvalidPlaygroundAddressString("Address string not of form a.b.c.d")
        
        parts = map(cls.ConvertStringPart, parts)
        
        return cls(parts[0], parts[1], parts[2], parts[3])
    
    def __init__(self, semester, group, individual, index):
        self._semester = semester
        self._group = group
        self._individual = individual
        self._index = index
        self._addressString = ".".join(map(str, self.toParts()))
        
    def semester(self): return self._semester
    def group(self): return self._group
    def individual(self): return self._individual
    def index(self): return self._index
    
    def toString(self):
        return self._addressString

    def toParts(self):
        return [self._semester, self._group, self._individual, self._index]
    
    def __repr__(self):
        return self.toString()
    
    def __str__(self):
        return self.toString()
    
    def __eq__(self, other):
        if isinstance(other, PlaygroundAddressType):
            return (self._semester == other._semester and 
                    self._group == other._group and
                    self._individual == other._individual and
                    self._index == other._index)
        elif isinstance(other, str):
            return self._addressString == other
        return False
    
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def __hash__(self):
        return self._addressString.__hash__()
    
    def __getitem__(self, i):
        if i < 0 or i > 3:
            raise IndexError("Playground Addresses have 4 parts")
        if i == 0: return self._semester
        if i == 1: return self._group
        if i == 2: return self._individual
        if i == 3: return self._index

class PlaygroundAddress(PlaygroundAddressType):
    @classmethod
    def ConvertStringPart(cls, part):
        return int(part)
    
    def __init__(self, semester, group, individual, index):
        super(PlaygroundAddress, self).__init__(semester, group, individual, index)
        
        self.validate(raiseException=True)
        
    def validate(self, raiseException=False):
        for part in self.toParts():
            if not type(part) == int or part < 0:
                raise InvalidPlaygroundFormat("Address parts must be positive integers")
    
class PlaygroundAddressBlock(PlaygroundAddressType):
    @classmethod
    def ConvertStringPart(cls, part):
        return part == "*" and part or int(part)
    
    def __init__(self, semester="*", group="*", individual="*", index="*"):
        super(PlaygroundAddressBlock,self).__init__(semester, group, individual, index)
        self.validate(raiseException=True)
        
    def validate(self, raiseException=False):
        for part in self.toParts():
            if not (part == "*" or (type(part) == int and part >=0)): 
                if raiseException:
                    raise InvalidPlaygroundFormat("Invalid part %s" % part)
                return False
        return True
            
    def spawnAddress(self, maxInt=((2**16)-1), addrType=PlaygroundAddress):
        addrParts = []
        for part in self.toParts():
            if part == "*":
                addrParts.append(random.randint(0, maxInt))
            else:
                addrParts.append(part)
        return addrType(*addrParts)
    
# figure out how IAddress works...
class PlaygroundAddressPair(object):
    def __init__(self, playgroundAddress, port):
        if not isinstance(playgroundAddress, PlaygroundAddress):
            playgroundAddress = PlaygroundAddress.FromString(playgroundAddress)
        if port < 0:
            raise Exception("Port must be positive")
        self.host = playgroundAddress
        self.port = port
        self.__stringValue = self.host.toString() + ": "+str(self.port)
        self.__hashValue = self.__stringValue.__hash__()

        
    def __eq__(self, other):
        if isinstance(other, PlaygroundAddressPair):
            return self.host == other.host and self.port == other.port
        return False
    
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def __hash__(self):
        return self.__hashValue
        
    def toString(self):
        return self.__stringValue
    
    def __repr__(self):
        return self.toString()
    
    def __str__(self):
        return self.toString()