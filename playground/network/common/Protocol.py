'''
Created on Oct 23, 2013

@author: sethjn
'''


import logging
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.Errors import DeserializationError,\
    UnexpectedMessageError
from playground.error.ErrorHandler import GetErrorReporter
from zope.interface.declarations import implements
from twisted.internet.interfaces import ITransport
from twisted.internet.defer import Deferred
from playground.network.common.Timer import callLater
from playground.playgroundlog import TaggedLogger
from playground import playgroundlog
from playground.network.common.Packet import PacketStorage

logger = logging.getLogger(__name__)
errReporter = GetErrorReporter(__name__)

class MessageStorage(object):
    
    def __init__(self, messageType=None, myErrReporter=None):
        # Allow an error reporter to be passed in. The calling
        # code will probably want to use its own error trapping
        # and reporting
        self.__errReporter = myErrReporter and myErrReporter or errReporter
        self.__messageType = messageType and messageType or MessageDefinition
        self.clear()
        
    def __forceAdvanceBufferPointer(self, offset=1):
        # we need to not keep trying with the current buffer
                # advance packetStorage at least one byte.
        if self.__packetStorage and self.__packetStorage[0]:
            self.__packetStorage[0] = self.__packetStorage[0][offset:]
        
    def update(self, data):
        if data: self.__packetStorage.append(data)
        
    def clear(self):
        self.__packetStorage = []
        self.__streamIterator = None
        
    def popMessage(self):
        dirty = False
        while self.__packetStorage:
            if not self.__packetStorage[0]:
                self.__packetStorage.pop(0)
                continue
            if not self.__streamIterator:
                self.__streamIterator = self.__messageType.DeserializeStream(self.__packetStorage)
            try:
                message = self.__streamIterator.next()
                dirty = False
            except StopIteration:
                message = None
                self.__streamIterator = None
                self.__errReporter.error("Could not get messageBuilder. This is unexpected.")
                self.__forceAdvanceBufferPointer()
                continue
            except DeserializationError, e:
                if not dirty:
                    # we have an error. It may take use some time to find the right place
                    # in the stream. Don't report the error until we're done or found a good spot
                    dirty = True
                    self.__errReporter.error("New Deserialization error. Attempt to get back in stream.", exception=e)
                self.__streamIterator = None
                self.__forceAdvanceBufferPointer()
                continue
            except UnexpectedMessageError, e:
                self.__errReporter.error("Deserialized the wrong type of message")
                continue
            except Exception, e:
                self.__errReporter.error("Unexpected error in deserialization.")
                
            if not message:
                # there shouldn't be any left over bytes
                # logger.debug("Remaining buffers lengths: %s" % map(len, self.__packetStorage))
                return None
            
            # TODO: Figure out a logging strategy.
            self.__streamIterator = None
            return message
        
    def iterateMessages(self):
        while True:
            message = self.popMessage()
            if message: yield message
            else: break

class StackingFactoryMixin(object):
    __higherFactory = None
    
    @classmethod
    def StackType(cls, higherFactoryType):
        class StackedType(cls):
            @classmethod
            def Stack(self, higherFactory):
                return cls.Stack(higherFactoryType.Stack(higherFactory))
        return StackedType
    
    @classmethod
    def Stack(cls, higherFactory):
        factory = cls()
        factory.setHigherFactory(higherFactory)
        return factory
    
    def setHigherFactory(self, f):
        """This is a Mixin Class. We have no constructor.
        However, this function must be called if it should behave different
        in "buildProtocol" so we can alter the function here.
        
        We save off buildProtocol to originalBuildProtocol and point
        both buildProtocol and buildProtocolStack to autoBuildProtocolStack.
        buildProtocolStack is legacy and should be removed.
        """
        self.buildProtocolStack = self.autoBuildProtocolStack
        self.originalBuildProtocol = self.buildProtocol
        self.buildProtocol = self.autoBuildProtocolStack
        self.__higherFactory = f
        
    def higherFactory(self):
        return self.__higherFactory
    
    def buildProtocolStack(self, addr):
        myProt = self.buildProtocol(addr)
        if self.higherFactory():
            if isinstance(self.higherFactory(), StackingFactoryMixin):
                higherProt = self.higherFactory().buildProtocolStack(addr)
            else:
                higherProt = self.higherFactory().buildProtocol(addr)
            myProt.setHigherProtocol(higherProt)
        return myProt
    
    def autoBuildProtocolStack(self, addr):
        myProt = self.originalBuildProtocol(addr)
        if self.higherFactory():
            if isinstance(self.higherFactory(), StackingFactoryMixin):
                higherProt = self.higherFactory().buildProtocolStack(addr)
            else:
                higherProt = self.higherFactory().buildProtocol(addr)
            myProt.setHigherProtocol(higherProt)
        return myProt
       
class StackingProtocolMixin(object):
    __higherProtocol = None
    __higherConnectionDeferred = None
    
    def waitForHigherConnection(self):
        self.__higherConnectionDeferred = Deferred()
        if self.higherProtocol().transport:
            callLater(0,self.__higherConnectionDeferred.callback, self.higherProtocol())
        return self.__higherConnectionDeferred
    
    def makeHigherConnection(self, higherTransport):
        self.higherProtocol().makeConnection(higherTransport)
        if self.__higherConnectionDeferred:
            callLater(0,self.__higherConnectionDeferred.callback, self.higherProtocol())
    
    def setHigherProtocol(self, higherProtocol):
        self.__higherProtocol = higherProtocol
        
    def higherProtocol(self):
        return self.__higherProtocol
        
    def applicationLayer(self):
        higherLayer = self.higherProtocol()
        if not higherLayer: return self
        
        if isinstance(higherLayer, StackingProtocolMixin): 
            return higherLayer.applicationLayer()
        return higherLayer
    
class StackingTransport(object):
    implements(ITransport)
    
    def __init__(self, lowerTransport):
        self.__lowerTransport = lowerTransport
        
    def lowerTransport(self):
        return self.__lowerTransport
        
    def write(self, data):
        self.__lowerTransport.write(data)
        
    def writeSequence(self, seq):
        self.__lowerTransport.writeSequence(seq)
        
    def getHost(self):
        return self.__lowerTransport.getHost()
    
    def getPeer(self):
        return self.__lowerTransport.getPeer()
    
    def loseConnection(self):
        return self.__lowerTransport.loseConnection()
    
    def __repr__(self):
        return "%s Transport %s to %s over %s" % (self.__class__,
                                                  self.getHost(), self.getPeer(),
                                                  self.__lowerTransport)
    
class ProtocolEvents(object):
    PROTOCOL_LISTENERS = {}
    EVENT_CONNECTION_MADE = 0
    EVENT_CONNECTION_LOST = 1
    EVENT_DATA_RECEIVED   = 2
    EVENT_DATA_SENT       = 3
    
    class ProtocolData(object):
        def __init__(self, protocol):
            self.originalConnectionMade = protocol.connectionMade
            self.originalConnectionLost = protocol.connectionLost
            self.originalDataReceived   = protocol.dataReceived
            self.transport              = None
            self.originalTransportWrite = None
            self.listeners = []
            
        def transportData(self, protocol):
            self.transport = protocol.transport
            self.originalTransportWrite = protocol.transport.write
            protocol.transport.write = lambda *args, **kargs: ProtocolEvents.protocolEvent(protocol, 
                                                                     self.originalTransportWrite,
                                                                     ProtocolEvents.EVENT_DATA_SENT,
                                                                     *args, **kargs)
            
        def restoreProtocol(self, protocol):
            protocol.connectionMade = self.originalConnectionMade
            protocol.connectionLost = self.originalConnectionLost
            protocol.dataReceived   = self.originalDataReceived
            if self.transport and self.originalTransportWrite:
                self.transport.write = self.originalTransportWrite
                self.transport = None
            self.originalTransportWrite = None
            self.originalConnectionLost = None
            self.originalConnectionMade = None
            self.originalDataReceived   = None
    
    @classmethod
    def protocolEvent(cls, protocol, protocolMethod, event, *args, **kargs):
        r = protocolMethod(*args, **kargs)
        protocolData = cls.PROTOCOL_LISTENERS.get(protocol, None)
        if not protocolData:
            return r
        
        for l in protocolData.listeners:
            l(protocol, event, args, kargs, r)
        if event == cls.EVENT_CONNECTION_MADE and protocol.transport:
            protocolData.transportData(protocol)
        if event == cls.EVENT_CONNECTION_LOST:
            protocolData.restoreProtocol(protocol)
            del cls.PROTOCOL_LISTENERS[protocol]
        return r
    
    @classmethod
    def EnableProtocol(cls, protocol):
        if not cls.PROTOCOL_LISTENERS.has_key(protocol):
            protocolData = cls.ProtocolData(protocol)
            cls.PROTOCOL_LISTENERS[protocol] = protocolData
            protocol.connectionMade = lambda *args, **kargs: cls.protocolEvent(protocol, 
                                                                               protocolData.originalConnectionMade, 
                                                                               cls.EVENT_CONNECTION_MADE, 
                                                                               *args, **kargs)
            protocol.connectionLost = lambda *args, **kargs: cls.protocolEvent(protocol, 
                                                                               protocolData.originalConnectionLost, 
                                                                               cls.EVENT_CONNECTION_LOST,
                                                                               *args, **kargs)
            protocol.dataReceived   = lambda *args, **kargs: cls.protocolEvent(protocol, protocolData.originalDataReceived,  
                                                                               cls.EVENT_DATA_RECEIVED,
                                                                               *args, **kargs)
        return protocol
    
    @classmethod
    def Listen(cls, protocol, listener):
        cls.EnableProtocol(protocol)
        cls.PROTOCOL_LISTENERS[protocol].listeners.append(listener)
    
    @classmethod
    def EnableProtocolClass(cls, protocolClass):
        raise Exception("Not yet implemented")    
        

class ProtocolLoggerAdapter(object):
    """
    These class-level operations: prevent memory leaks.
    
    Consider. If we created an adapter for each protocol with a pointer
    to the protocol and then had the protocol have a pointer to the adapter
    we'd have a circular dependency and a memory leak.
    
    so, instead, we have to custom adapt the protocol so that 
    connectionLost clears us. We have to keep track
    of the adapters in case a protocol calls us twice.
    """
    
    def __init__(self, protocol):
        self.protocolClass, self.protocolId = protocol.__class__, id(protocol)
        self.transport = None
        ProtocolEvents.Listen(protocol, self.protocolEventListener)
        
    def protocolEventListener(self, protocol, protocolMethod, args, kargs, rValue):
        if protocolMethod == protocol.connectionMade:
            self.transport = protocol.transport
        elif protocolMethod == protocol.connectionLost:
            self.transport = None
    
    def process(self, msg, kwargs):
        pmsg = "[%s (%d) connected to " % (self.protocolClass, self.protocolId)
        if self.transport:
            pmsg += str(self.transport.getPeer())
        else: pmsg += "<UNCONNECTED>"
        pmsg += "] "
        return pmsg + msg, kwargs
    
class ENABLE_PACKET_TRACING(object):
    TAG = "packettrace"
    DATA_STORAGE = {}
    
    @classmethod
    def FormatPacketData(cls, dataObj):
        protocol, data, direction = dataObj
        if not cls.DATA_STORAGE.has_key(protocol):
            return "<LOST PACKET DATA : No Protocol>"
        cls.DATA_STORAGE[protocol].update(data)
        msgString = ""
        for packet in cls.DATA_STORAGE[protocol].iterateMessages():
            msgType, msgVersion = packet.__class__.PLAYGROUND_IDENTIFIER, packet.__class__.MESSAGE_VERSION
            msgString = "<<PACKET %s by Protocol %s>> [" % (direction, protocol)
            msgString += msgType + " v"
            msgString += msgVersion + " ID:"
            msgString += str(packet.playground_msgID) +"] "
            msgString += "\n\t"
        if not msgString:
            msgString = "<<DATA RECEIVED BUT NOT PACKETS>>"
        return msgString
    
    def __init__(self, protocol, logLevel=logging.DEBUG, wireProtocol=False):
        ProtocolEvents.Listen(protocol, self)
        
        self.logLevel = logLevel
        self.packetTracingLoggerName = TaggedLogger.GetTaggedLoggerNameForObject(protocol, self.TAG)
        self.traceLogger = logging.getLogger(self.packetTracingLoggerName)
        ENABLE_PACKET_TRACING.DATA_STORAGE[protocol] = wireProtocol and PacketStorage() or MessageStorage()
        
    def __call__(self, protocol, eventType, args, kargs, r):
        if eventType == ProtocolEvents.EVENT_DATA_RECEIVED:
            direction = 'DATA RECEIVED'
        elif eventType == ProtocolEvents.EVENT_DATA_SENT:
            direction = 'TRANSPORT WRITE'
        elif eventType == ProtocolEvents.EVENT_CONNECTION_LOST:
            if ENABLE_PACKET_TRACING.DATA_STORAGE.has_key(protocol):
                del ENABLE_PACKET_TRACING.DATA_STORAGE[protocol]
                return 
        else:
            return
        
        data = args[0]
        specialData = {"packet_trace":(protocol, data, direction)}
        r = self.traceLogger.makeRecord(self.packetTracingLoggerName, 
                                    self.logLevel, "%s.dataReceived"%protocol, 0, 
                                    "%(packet_trace)s", [],
                                    None)
        r.__playground_special__ = specialData
        self.traceLogger.handle(r)
playgroundlog.PlaygroundLoggingFormatter.SPECIAL_CONVERTERS["packet_trace"] = ENABLE_PACKET_TRACING.FormatPacketData